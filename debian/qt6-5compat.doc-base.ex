Document: qt6-5compat
Title: Debian qt6-5compat Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-5compat is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-5compat/qt6-5compat.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-5compat/qt6-5compat.ps.gz

Format: text
Files: /usr/share/doc/qt6-5compat/qt6-5compat.text.gz

Format: HTML
Index: /usr/share/doc/qt6-5compat/html/index.html
Files: /usr/share/doc/qt6-5compat/html/*.html
